﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Addons.Interactive;

namespace LorinthsLair.Calculator.Bot.Modules
{
    public class UserCommands : InteractiveBase<SocketCommandContext>
    {
        private readonly Calculator _calculator = new Calculator();

        [Command("base", RunMode = RunMode.Async)]
        public async Task CalculateBaseFormula()
        {
            Tuple<double, double> baseXp;
            int interval, maxLevel;
            string baseFormula;
            SocketMessage userResponse;

            await ReplyAsync("What is the mob formula (config.yml Entity section)?");
            userResponse = await NextMessageAsync(true, true, TimeSpan.FromMinutes(5));
            if (userResponse == null)
            {
                await ReplyAsync("You run out of time");
                return;
            }
            baseFormula = userResponse.Content;

            await ReplyAsync("For which mob level's interval you would like to calculate mob values?");
            userResponse = await NextMessageAsync(true, true, TimeSpan.FromMinutes(5));
            if (userResponse == null)
            {
                await ReplyAsync("You run out of time");
                return;
            }
            interval = int.Parse(userResponse.Content);

            await ReplyAsync("To which level should it calculate?");
            userResponse = await NextMessageAsync(true, true, TimeSpan.FromMinutes(5));
            if (userResponse == null)
            {
                await ReplyAsync("You run out of time");
                return;
            }
            maxLevel = int.Parse(userResponse.Content);

            string message = "Base values:\n";

            for (int i = 0; i <= maxLevel; i += interval)
            {
                baseXp = _calculator.CalculateBase(baseFormula, i);
                message += $"Level: {i} lvl \t Min: {FormulaParser.ParseResult(baseXp.Item1, false)} \t Max: {FormulaParser.ParseResult(baseXp.Item2, true)}\n";
            }

            message += "Values can be different from real by +/- 1.";

            await ReplyAsync(message);
        }

        [Command("bonus", RunMode = RunMode.Async)]
        [Alias("overall")]
        public async Task CalculateBonusFormula()
        {
            Tuple<double, double> baseXp, bonus;
            int mobLevel, playerLevel;
            string baseFormula, bonusFormula;
            SocketMessage userResponse;

            //Get Values
            await ReplyAsync("What is the mob level?");
            userResponse = await NextMessageAsync(true, true, TimeSpan.FromMinutes(5));
            if (userResponse == null)
            {
                await ReplyAsync("You run out of time");
                return;
            }
            mobLevel = int.Parse(userResponse.Content);

            await ReplyAsync("\nWhat is the player level?");
            userResponse = await NextMessageAsync(true, true, TimeSpan.FromMinutes(5));
            if (userResponse == null)
            {
                await ReplyAsync("You run out of time");
                return;
            }
            playerLevel = int.Parse(userResponse.Content);

            await ReplyAsync("\nWhat is the mob experience formula (config.yml Entity section)?");
            userResponse = await NextMessageAsync(true, true, TimeSpan.FromMinutes(5));
            if (userResponse == null)
            {
                await ReplyAsync("You run out of time");
                return;
            }
            baseFormula = userResponse.Content;

            if (mobLevel > playerLevel)
            {
                await ReplyAsync("\nWhat is the MobLevelHigher formula (formulas.yml)?");
            }
            else if (mobLevel == playerLevel)
            {
                await ReplyAsync("\nWhat is the MobLevelEqual formula (formulas.yml)?");
            }
            else
            {
                await ReplyAsync("\nWhat is the MobLevelLower formula (formulas.yml)?");
            }

            userResponse = await NextMessageAsync(true, true, TimeSpan.FromMinutes(5));
            if (userResponse == null)
            {
                await ReplyAsync("You run out of time");
                return;
            }
            bonusFormula = userResponse.Content;

            //Calculate base
            baseXp = _calculator.CalculateBase(baseFormula, mobLevel);

            //Calculate Bonus
            bonus = _calculator.CalculateBonus(bonusFormula, mobLevel, playerLevel, baseXp);

            //Show results
            await ReplyAsync($@"

Base experience:
Min: {FormulaParser.ParseResult(baseXp.Item1, false)} xp
Max: {FormulaParser.ParseResult(baseXp.Item2, true)} xp

Bonus:
Min: {FormulaParser.ParseResult(bonus.Item1, false)} xp
Max: {FormulaParser.ParseResult(bonus.Item2, true)} xp

Overall:
Min: {FormulaParser.ParseResult((baseXp.Item1 + bonus.Item1), false)} xp
Max: {FormulaParser.ParseResult((baseXp.Item2 + bonus.Item2), true)} xp

Values can be different from real by +/- 1.");
        }
    }
}
