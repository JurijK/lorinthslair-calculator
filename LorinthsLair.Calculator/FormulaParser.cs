﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace LorinthsLair.Calculator
{
    public class FormulaParser
    {
        public static string ParseBaseFormula(string formula, int mobLevel)
        {
            return formula.Replace("{level}", $"{mobLevel}");
        }

        public static string ParseBonusFormula(string formula, int mobLevel, int playerLevel, double experience)
        {
            return formula.Replace("{exp}", experience.ToString(CultureInfo.InvariantCulture))
                .Replace("{mobLevel}", $"{mobLevel}")
                .Replace("{playerLevel}", $"{playerLevel}");
        }

        public static string ParseResult(double result, bool IsMax)
        {
            if (IsMax)
            {
                return Math.Ceiling(result).ToString(CultureInfo.InvariantCulture);
            }

            return Math.Floor(result).ToString(CultureInfo.InvariantCulture);
        }
    }
}
