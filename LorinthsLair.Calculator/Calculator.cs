﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace LorinthsLair.Calculator
{
    /// <summary>
    /// Used to do math evaluations of string formulas
    /// </summary>
    /// <remarks>
    /// Originaly created by LorinthsLairDevelopment team
    /// https://bitbucket.org/lorinthslairdevelopment/lorinthsutils/src/develop/
    /// </remarks>
    public class Calculator
    {
        private int character;
        private int position = -1;
        private readonly Random _random = new Random(DateTime.Now.Millisecond);
        private string formula;

        public Tuple<double, double> CalculateBase(string baseFormula, int mobLevel)
        {
            double minBase, maxBase;

            minBase = Eval(FormulaParser.ParseBaseFormula(baseFormula, mobLevel), false);
            maxBase = Eval(FormulaParser.ParseBaseFormula(baseFormula, mobLevel), true);

            return new Tuple<double, double>(minBase, maxBase);
        }

        public Tuple<double, double> CalculateBonus(string bonusFormula, int mobLevel, int playerLevel, Tuple<double, double> baseXp)
        {
            double minBonus, maxBonus;

            minBonus = Eval(FormulaParser.ParseBonusFormula(bonusFormula, mobLevel, playerLevel, baseXp.Item1), false);
            maxBonus = Eval(FormulaParser.ParseBonusFormula(bonusFormula, mobLevel, playerLevel, baseXp.Item2), true);

            return new Tuple<double, double>(minBonus, maxBonus);
        }

        private string PreparseFormula(string formula)
        {
            return formula.Replace("math.floor(math.random() * ", "rand(")
                .Replace("math.min(", "min(")
                .Replace("math.min(", "min(")
                .Replace("math.max(", "max(")
                .Replace("math.max(", "max(");
        }

        private double Eval(string formula, bool maximalize)
        {
            this.formula = PreparseFormula(formula.ToLower().Replace(" ", ""));

            this.character = 0;
            this.position = -1;

            return Parse(maximalize);
        }

        private void NextChar()
        {
            this.character = (++this.position < this.formula.Length) ? this.formula[position] : -1;
        }

        private bool Eat(int charToEat)
        {
            while (this.character == ' ')
            {
                NextChar();
            }

            if (this.character == charToEat)
            {
                NextChar();
                return true;
            }

            return false;
        }

        private double Parse(bool maximalize)
        {
            NextChar();
            double result = ParseExpression(maximalize);
            if (position < formula.Length)
            {
                throw new ArgumentException("Unexpected: " + (char)this.character + " at index " + this.position + " in formula: " + this.formula);
            }
            return result;
        }

        private string GetFullParameterString()
        {
            int openParens = 1;
            for (int i = this.position; i < this.formula.Length; i++)
            {
                char cha = this.formula[i];

                if (cha == '(') openParens++;
                else if (cha == ')') openParens--;

                if (openParens == 0)
                {
                    string parameters = this.formula.Substring(this.position, i - this.position);
                    this.position = i;
                    this.character = ')';
                    return parameters;
                }
            }
            throw new ArgumentException("The function has no closing brace in, " + this.formula);
        }

        private List<string> GetParameters(string fullParameterString)
        {
            List<string> parameters = new List<string>();

            int openParens = 0;
            int start = 0;
            for (int i = 0; i < fullParameterString.Length; i++)
            {
                char cha = fullParameterString[i];

                if (cha == ',' && openParens == 0)
                {
                    parameters.Add(fullParameterString.Substring(start, i - start));
                    start = i + 1;
                }
                else if (cha == '(')
                {
                    openParens++;
                }
                else if (cha == ')')
                {
                    openParens--;
                }
            }

            if (start != 0)
            {
                parameters.Add(fullParameterString.Substring(start));
            }

            if (parameters.Count == 0)
            {
                parameters.Add(fullParameterString);
            }

            return parameters;
        }

        private double ParseExpression(bool maximalize)
        {
            double result = ParseTerm(maximalize);
            for (; ; )
            {
                if (Eat('+')) result += ParseTerm(maximalize); // addition
                else if (Eat('-')) result -= ParseTerm(maximalize); // subtraction
                else return result;
            }
        }

        private CalculatorResult ParseMultipleArgumentExpressions(bool maximalize)
        {
            CalculatorResult result = new CalculatorResult(0);

            string paramString = GetFullParameterString();
            List<string> parameters = GetParameters(paramString);
            if (parameters.Count == 0)
                return result;

            result.value = new Calculator().Eval(parameters[0], maximalize);

            for (int i = 0; i < parameters.Count; i++)
            {
                result.values.Add(new Calculator().Eval(parameters[i].Trim(), maximalize));
            }

            return result;
        }

        private double ParseTerm(bool maximalize)
        {
            double result = ParseFactor(maximalize);
            for (; ; )
            {
                if (Eat('*')) result *= ParseFactor(maximalize); // multiplication
                else if (Eat('/')) result /= ParseFactor(maximalize); // division
                else return result;
            }
        }

        private double ParseFactor(bool maximalize)
        {
            if (Eat('+')) return ParseFactor(maximalize); // unary plus
            if (Eat('-')) return -ParseFactor(maximalize); // unary minus

            double x;
            int startPos = this.position;
            if (Eat('('))
            { // parentheses
                x = ParseExpression(maximalize);
                Eat(')');
            }
            else if ((this.character >= '0' && this.character <= '9') || this.character == '.')
            { // numbers
                while ((this.character >= '0' && this.character <= '9') || this.character == '.') NextChar();
                x = Double.Parse(this.formula.Substring(startPos, this.position - startPos), CultureInfo.InvariantCulture);
            }
            else if (this.character >= 'a' && this.character <= 'z')
            { // functions
                while (this.character >= 'a' && this.character <= 'z') NextChar();
                String func = this.formula.Substring(startPos, this.position - startPos).ToLower();
                Eat('(');
                CalculatorResult result = ParseMultipleArgumentExpressions(maximalize);
                x = result.value;
                Eat(')');
                if (func.Equals("sqrt")) x = Math.Sqrt(x);
                else if (func.Equals("sin")) x = Math.Sin(ToRadians(x));
                else if (func.Equals("cos")) x = Math.Cos(ToRadians(x));
                else if (func.Equals("tan")) x = Math.Tan(ToRadians(x));
                else if (func.Equals("random") || func.Equals("rand")) x = ParseRandom(result, maximalize);
                else if (func.Equals("min")) x = ParseMinimum(result.values);
                else if (func.Equals("max")) x = ParseMaximum(result.values);
                else if (func.Equals("abs")) x = ParseAbsoluteValue(result.value);
                else if (func.Equals("ln")) x = ParseNaturalLogarithm(result.value);
                else if (func.Equals("log")) x = ParseLogarithm(result);
                else if (func.Equals("exp")) x = ParseExp(result.value);
                else if (func.Equals("floor")) x = ParseFloor(result.value);
                else if (func.Equals("ceil")) x = ParseCeil(result.value);
                else if (func.Equals("mod")) x = ParseModulo(result.values);
            }
            else
            {
                throw new ArgumentException("Unexpected: " + (char)this.character + " at index " + this.position);
            }

            if (Eat('^')) x = Math.Pow(x, ParseFactor(maximalize)); // exponentiation
            return x;
        }

        private double ParseRandom(CalculatorResult result, bool maximalize)
        {
            if (result.values.Count == 1)
            {
                int range = (int)result.value;
                if (range <= 0)
                    return 0;

                return maximalize ? (int)result.value : 0;
            }
            else if (result.values.Count == 2)
            {
                int a = (int)result.values[0];
                int b = (int)result.values[1];
                int min = Math.Min(a, b);
                int max = Math.Max(a, b);
                return maximalize ? max - 1 : min;
            }
            return 0;
        }

        private double ParseMinimum(List<Double> args)
        {
            if (args.Count == 0)
                return 0;

            Double min = args[0];
            for (int i = 1; i < args.Count; i++)
            {
                min = Math.Min(min, args[i]);
            }

            return min;
        }

        private double ParseMaximum(List<Double> args)
        {
            if (args.Count == 0)
                return 0;

            Double max = args[0];
            for (int i = 1; i < args.Count; i++)
            {
                max = Math.Max(max, args[i]);
            }

            return max;
        }

        private double ParseAbsoluteValue(double value)
        {
            return Math.Abs(value);
        }

        private double ParseNaturalLogarithm(double value)
        {
            if (value <= 0) throw new ArgumentException("The arguments in ln must be positive number");
            return Math.Log(value);
        }

        private double ParseLogarithm(CalculatorResult result)
        {
            double numberOf;
            double logBase;

            if (result.values.Count == 1)
            {
                numberOf = result.value;
                logBase = 10.0;
            }
            else if (result.values.Count == 2)
            {
                numberOf = result.values[0];
                logBase = result.values[1];
            }
            else
                return 0;

            if (logBase == 1.0) throw new ArgumentException("The logarithm base can't be 1");

            return Math.Log(numberOf, logBase);
        }

        private double ParseExp(double value)
        {
            return Math.Exp(value);
        }

        private double ParseFloor(double value)
        {
            return Math.Floor(value);
        }

        private double ParseCeil(double value)
        {
            return Math.Ceiling(value);
        }

        private double ParseModulo(List<double> values)
        {
            if (values.Count != 2) throw new ArgumentException("mod() must take 2 arguments");

            double number = values[0];
            double modulus = values[1];

            return number % modulus;
        }

        public static double ToRadians(double degrees)
        {
            return degrees * Math.PI / 180.0;
        }
    }
}

