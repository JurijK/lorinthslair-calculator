﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace LorinthsLair.Calculator
{
    public class CalculatorResult
    {
        public double value;
        public List<double> values;

        public CalculatorResult(double value)
        {
            this.value = value;
            this.values = new List<double>();
        }

        public CalculatorResult(double value, List<double> values)
        {
            this.value = value;
            this.values = values ?? new List<double>();
        }

        public override String ToString()
        {
            return "{value: " + this.value + ", values: " + (this.values != null ? this.values.ToString() : "") + "}";
        }
    }
}
