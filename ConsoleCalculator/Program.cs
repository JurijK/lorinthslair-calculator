﻿using System;
using System.Globalization;
using LorinthsLair.Calculator;

namespace ConsoleCalculator
{
    internal class Program
    {
        private static readonly Calculator _calculator = new Calculator();

        private static void Main()
        {
            try
            {
                while (true)
                {
                    MainMenu();
                    Console.ReadKey();
                    Console.Clear();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"ERROR! Press any button to close program.\n\nError reason:\n{ex}");
                Console.ReadKey();
                Environment.Exit(-1);
            }
        }

        private static void MainMenu()
        {
            Console.WriteLine(@"MAIN MENU:
1. Calculate base values
2. Calculate overall experience
3. Exit");

            int choice = int.Parse(Console.ReadLine());

            Console.Clear();

            switch (choice)
            {
                case 1:
                    CalculateBaseValues();
                    break;
                case 2:
                    CalculateOverallExperience();
                    break;
                case 3:
                    Environment.Exit(0);
                    break;
                default:
                    break;
            }
        }

        private static void CalculateBaseValues()
        {
            Tuple<double, double> baseValues;
            int interval, maxLevel;
            string baseFormula;

            Console.WriteLine("What is the mob's formula (config.yml Entity section)?");
            baseFormula = Console.ReadLine();
            Console.WriteLine("\nFor which mob level's interval you would like to calculate mob's values?");
            interval = int.Parse(Console.ReadLine());
            Console.WriteLine("\nTo which level should it calculate?");
            maxLevel = int.Parse(Console.ReadLine());

            Console.WriteLine($"\n\nBase Values:");

            for (int i = 0; i <= maxLevel; i += interval)
            {
                baseValues = _calculator.CalculateBase(baseFormula, i);
                Console.WriteLine($"Level: {i} lvl\tMin: {FormulaParser.ParseResult(baseValues.Item1, false)}\tMax: {FormulaParser.ParseResult(baseValues.Item2, true)}");
            }

            Console.WriteLine("\nValues can be different from real by +/- 1.");
        }

        private static void CalculateOverallExperience()
        {
            Tuple<double, double> baseXp, bonus;
            int mobLevel, playerLevel;
            string baseFormula, bonusFormula;

            //Get Values
            Console.WriteLine("What is the mob level?");
            mobLevel = int.Parse(Console.ReadLine());
            Console.WriteLine("\nWhat is the player level?");
            playerLevel = int.Parse(Console.ReadLine());
            Console.WriteLine("\nWhat is the mob experience formula (config.yml Entity section)?");
            baseFormula = Console.ReadLine();

            if (mobLevel > playerLevel)
            {
                Console.WriteLine("\nWhat is the MobLevelHigher formula (formulas.yml)?");
                bonusFormula = Console.ReadLine();
            }
            else if (mobLevel == playerLevel)
            {
                Console.WriteLine("\nWhat is the MobLevelEqual formula (formulas.yml)?");
                bonusFormula = Console.ReadLine();
            }
            else
            {
                Console.WriteLine("\nWhat is the MobLevelLower formula (formulas.yml)?");
                bonusFormula = Console.ReadLine();
            }

            //Calculate base
            baseXp = _calculator.CalculateBase(baseFormula, mobLevel);

            //Calculate Bonus
            bonus = _calculator.CalculateBonus(bonusFormula, mobLevel, playerLevel, baseXp);

            //Show results
            Console.WriteLine($@"

Base experience:
Min: {FormulaParser.ParseResult(baseXp.Item1, false)} xp
Max: {FormulaParser.ParseResult(baseXp.Item2, true)} xp

Bonus:
Min: {FormulaParser.ParseResult(bonus.Item1, false)} xp
Max: {FormulaParser.ParseResult(bonus.Item2, true)} xp

Overall:
Min: {FormulaParser.ParseResult((baseXp.Item1 + bonus.Item1), false)} xp
Max: {FormulaParser.ParseResult((baseXp.Item2 + bonus.Item2), true)} xp

Values can be different from real by +/- 1.");
        }
    }
}
